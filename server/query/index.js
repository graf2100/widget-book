module.exports = {
  GET_ROOM:
    `query getRoom($id: Int) {
      room(id : $id) {
         name
         city
         address
         type
         category
         players_min
         players_max
         timelines {
           time_open
           time_play
           time_break
           count_game
           days
           price
           player_price
           type
        }
      }
    }
  `
}