const app = require('express')()
const express = require('express')
const path = require('path')
const http = require('http').Server(app)
const io = require('socket.io')(http)
const Booking = require('./booking')


app.use(express.static(path.join(__dirname, '..', 'build')))
app.get('*', (req, res) => {
  res.sendFile(path.join(path.join(__dirname, '..', 'build', 'index.html')))
})
let booking = new Booking()
io.on('connection', function (socket) {

  socket.on('disconnect', reason => {
    console.log(reason)
  })

  socket.on('room', async id => {
    socket.emit('room', await booking.getRoom(id))
  })

  socket.on('error', error => {
    console.error(error)
  })
})

http.listen(4000, function () {
  console.log('listening on *:4000')
})