const { request } = require('graphql-request')
const config = require('../config')
const { GET_ROOM } = require('../query')

class Booking {
  constructor () {
    this.active = []
  }

  addActive (room, time) {
    if (room && time) {
      this.active.push({ room, time })
    }
  }

  booking (room, time) {
    console.log(room, time)
  }

  async getRoom (id) {
    try {
      let data = await request(config.URL_SERVER, GET_ROOM, { id })
      let { room } = data
      return room
    } catch (e) {
      console.log('Error get room')
      return []
    }

  }

  cancelActive (room, time) {
    if (room && time) {
      this.active = this.active.filter(a => a.room !== room && a.time !== time)
    }
  }
}

module.exports = Booking