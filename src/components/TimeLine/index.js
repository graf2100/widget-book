import React     from 'react'
import PropTypes from 'prop-types'

import { withStyles } from '@material-ui/core/styles'
import Grid           from '@material-ui/core/Grid'
import Typography     from '@material-ui/core/Typography'

const styles = theme => ({

  container: {
    display: 'flex',
    width: '100%',
    flexDirection: 'column',
    textAlign: 'center'
  },
  item: {
    margin: 8,
    height: 36,
    width: '100%',
  },
  content: {
    padding: '6px 16px'
  }
})

function TimeLine (props) {
  let {periods, classes} = props
  return (
    <Grid item lg={1} xs={1}>
      <div className={classes.container}>
        {periods.map(period => (
          <div className={classes.item} key={period}>
            <div className={classes.content}>
              <Typography variant="title" gutterBottom>{period}</Typography>
            </div>
          </div>
        ))}
      </div>
    </Grid>)
}

TimeLine.propTypes = {
  periods: PropTypes.arrayOf(PropTypes.string),
  classes: PropTypes.object.isRequired,
}
TimeLine.defaultProps = {
  periods: ['10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00',]
}

export default withStyles(styles)(TimeLine)