import React, { Fragment } from 'react'
import PropTypes           from 'prop-types'

import Dialog       from '@material-ui/core/Dialog'
import DialogTitle  from '@material-ui/core/DialogTitle'
import List         from '@material-ui/core/List'
import ListItem     from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import Countries    from '../../utilities/countries'
import ExpandMore   from '@material-ui/icons/ExpandMore'

import { withStyles } from '@material-ui/core/styles'

const styles = () => ({
  blockCountry: {
    marginTop: 16,
    marginBottom: 8,
    padding: '11.5px 14px',
    fontSize: '1.5rem',
    transition: 'padding-left 200ms cubic-bezier(0.0, 0, 0.2, 1) 0ms,border-color 200ms cubic-bezier(0.0, 0, 0.2, 1) 0ms,border-width 200ms cubic-bezier(0.0, 0, 0.2, 1) 0ms',
    borderStyle: 'solid',
    borderWidth: 1,
    borderRadius: 4,
    paddingRight: 4,
    borderColor: 'rgba(0, 0, 0, 0.23)',
    '&:hover': {
      borderColor: 'rgba(0, 0, 0, 0.87)'
    }
  },
  arrayDown: {
    color: 'rgba(0, 0, 0, 0.23)',
    width: '1.2em',
    position: 'relative',
    top: '.2rem'
  },
  dialog: {
    borderRadius: 0
  }
})

function SelectCountries (props) {
  let {classes, open, handleClose, handleOpen, index} = props
  return (
    <Fragment>
      <div className={classes.blockCountry} onClick={handleOpen}>
        {Countries[index].flag} <ExpandMore className={classes.arrayDown}/>
      </div>
      <Dialog aria-labelledby="simple-dialog-title" classes={{paper: classes.dialog}}
              onClose={() => handleClose(index)}
              open={open}>
        <DialogTitle id="simple-dialog-title">Выберите страну</DialogTitle>
        <div>
          <List component="nav">
            {Countries.map((country, i) =>
              (<ListItem key={country.name} button onClick={() => handleClose(i)}>
                <ListItemText primary={country.flag + '   ' + country.name}/>
              </ListItem>))}
          </List>
        </div>
      </Dialog>
    </Fragment>)
}

SelectCountries.propTypes = {
  classes: PropTypes.object.isRequired,
  index: PropTypes.number,
  open: PropTypes.bool,
  handleClose: PropTypes.func,
  handleOpen: PropTypes.func
}

SelectCountries.defaultProps = {
  index: 0,
  open: true,
  handleClose: (i) => console.log(i)
}

export default withStyles(styles)(SelectCountries)