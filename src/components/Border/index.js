import React from 'react'
import PropTypes from 'prop-types'

import { withStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Grey from '@material-ui/core/colors/grey'
import { IButton } from '../../components'

const styles = () => ({
  container: {
    display: 'flex',
    width: '100%'
  },
  item: {
    margin: 8,
    width: `calc(${100 / 7}% - 16px)`,
  },
  root: {
    background: Grey[100],
  },
})

function controller (period, day, scheme, key, classes, handleOpen) {
  let item = scheme.find(s => s.time === period && s.day === day)
  if (item) {
    return (<div key={key} className={classes.item}><IButton value={item.price} onOpen={handleOpen}/></div>)
  } else {
    item = scheme.find(s => s.time === period)
    return (
      <div key={key} className={classes.item}><IButton value={item.price} isActive={false} onOpen={handleOpen}/></div>)
  }
}

function Border (props) {
  let { classes, handleOpen, periods, days, scheme } = props
  return (
    <Grid item lg={7} xs={11} className={classes.root}>
      {periods.map((period, index) => {
        return (
          <div key={'grid' + index} className={classes.container}>
            {days.map(day => controller(period, day, scheme, period + day, classes, handleOpen))}
          </div>)
      })}
    </Grid>)
}

Border.propTypes = {
  classes: PropTypes.object.isRequired,

  items: PropTypes.arrayOf(PropTypes.shape({
    date: PropTypes.object,
    timer: PropTypes.number,
    other: PropTypes.arrayOf(PropTypes.shape({
      name: PropTypes.string,
      price: PropTypes.number
    })),
    periods: PropTypes.arrayOf(PropTypes.string),
    price: PropTypes.number,
    additional: PropTypes.number,
    days: PropTypes.arrayOf(PropTypes.number),
    scheme: PropTypes.arrayOf(PropTypes.shape({
      day: PropTypes.number,
      price: PropTypes.number,
      isActive: PropTypes.bool,
      time: PropTypes.string
    }))
  })),
  handleOpen: PropTypes.func
}
Border.defaultProps = {
  count: 0
}

export default withStyles(styles)(Border)