import React, { Fragment } from 'react'
import PropTypes from 'prop-types'

import * as moment from 'moment'
import 'moment/locale/ru'

import { withStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import IconButton from '@material-ui/core/IconButton'
import Typography from '@material-ui/core/Typography'
import Hidden from '@material-ui/core/Hidden'
import { NavigateBefore, NavigateNext } from '@material-ui/icons'

moment.locale('ru')

const styles = theme => ({
  container: {
    display: 'flex',
    width: '100%'
  },
  item: {
    margin: 8,
    marginTop: 4,
    marginBottom: 4,
    width: `calc(${100 / 7}% - 16px)`,
    textAlign: 'center'
  },
  arrayLeft: {
    top: '3.5rem',
    left: '-3%',
  },
  arrayRight: {
    display: 'block',
    position: 'absolute',
    top: '9rem',
    left: '81%',
    [theme.breakpoints.down('lg')]: {
      left: '95%',
    }, [theme.breakpoints.up('lg')]: {
      left: '81%',
    },
  }
  ,
  title: {
    margin: 0,
    textTransform: 'uppercase'
  }
})

function DateBar (props) {
  let { classes, active, next, prev } = props
  let arr = ['', '', '', '', '', '', '']
  return (
    <Fragment>
      <Grid item lg={3} xs={1}/>
      <Grid item lg={7} xs={11}>
        <IconButton className={classes.arrayLeft} onClick={prev}>
          <NavigateBefore fontSize="large"/>
        </IconButton>
        <div className={classes.container}>
          {arr.map((a, index) => (
            <div key={moment(active).add(index, 'd').format('l')} className={classes.item}>
              <Typography variant="title" className={classes.title}
                          gutterBottom>{moment(active).add(index, 'd').format('ddd')}</Typography>
              <Typography variant="subheading"
                          gutterBottom>{moment(active).add(index, 'd').format('DD MMMM')}</Typography>
            </div>))}
        </div>
        <IconButton className={classes.arrayRight} onClick={next}>
          <NavigateNext fontSize="large"/>
        </IconButton>
      </Grid>
      <Hidden only={['sm', 'xs', 'md']}>
        <Grid item lg={2}/>
      </Hidden>
    </Fragment>)
}

DateBar.propTypes = {
  classes: PropTypes.object.isRequired,
  now: PropTypes.object.isRequired,
  active: PropTypes.object.isRequired,
  next: PropTypes.func,
  prev: PropTypes.func
}
DateBar.defaultProps = {}

export default withStyles(styles)(DateBar)