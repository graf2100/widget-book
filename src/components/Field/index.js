import React     from 'react'
import PropTypes from 'prop-types'

import TextField      from '@material-ui/core/TextField'
import Purple         from '@material-ui/core/colors/purple'
import { withStyles } from '@material-ui/core/styles/index'

const styles = theme => ({
  checked: {
    color: Purple[300],
    '&$checked': {
      color: Purple[200],
    },
  },
  textField: {
    width: '100%',
    '&$cssFocused': {
      borderColor: Purple[300],
    },
  },
  cssLabel: {

    '&$cssFocused': {
      color: Purple[300],
      borderBottomColor: Purple[300],
    },
  },
  cssFocused: {},
  cssUnderline: {
    '&:after': {
      borderBottomColor: Purple[300],
    },
  },
  cssOutlinedInput: {
    borderColor: Purple[300],
    '&$cssFocused $notchedOutline': {
      borderColor: Purple[300],
    },
  },
  notchedOutline: {},

})

function Field (props) {
  let {label, classes, value, type, error, handleChange} = props
  return (<TextField
    label={label}
    className={classes.textField}
    InputLabelProps={{
      classes: {
        root: classes.cssLabel,
        focused: classes.cssFocused,
      },
    }}
    InputProps={{
      classes: {
        root: classes.cssOutlinedInput,
        focused: classes.cssFocused,
        notchedOutline: classes.notchedOutline,
      },
    }}
    type={type}
    value={value}
    margin="normal"
    variant="outlined"
    error={error}
    onChange={handleChange}
  />)
}

Field.propTypes = {
  label: PropTypes.string,
  type: PropTypes.string,
  value: PropTypes.string,
  error: PropTypes.bool,
  classes: PropTypes.object.isRequired,
  handleChange: PropTypes.func
}

Field.defaultProps = {
  error: false,
  type: 'text'
}

export default withStyles(styles)(Field)