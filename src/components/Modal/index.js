import React, { Component } from 'react'
import PropTypes from 'prop-types'

import Countries from '../../utilities/countries'
import { withStyles } from '@material-ui/core/styles'
import Modal from '@material-ui/core/Modal'
import Typography from '@material-ui/core/Typography'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Checkbox from '@material-ui/core/Checkbox'
import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid'

import { Field, SelectCountries } from '../../components'

import Purple from '@material-ui/core/colors/purple'
import Red from '@material-ui/core/colors/red'
import Orange from '@material-ui/core/colors/orange'

const styles = theme => ({
  paper: {
    position: 'absolute',
    width: theme.spacing.unit * 50,
    top: '3ch',
    margin: '0 calc(50% - 200px)',
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4,
    outline: 'none',
    [theme.breakpoints.down('sm')]: {
      margin: '0 2.5%',
      width: '80%',
    }, [theme.breakpoints.up('sm')]: {
      width: theme.spacing.unit * 50,
      margin: '0 calc(50% - 200px)',
    },
  },
  title: {
    textAlign: 'center',
    marginBottom: 8
  },
  text: {
    fontWeight: 300
  },
  checkBox: {
    width: '100%',
  },
  checked: {
    color: Purple[300],
    '&$checked': {
      color: Purple[200],
    },
  },
  link: {
    textDecoration: 'blink',
    color: Red[500]
  },
  buttonBuy: {
    color: '#ffff',
    backgroundColor: Purple[300],
    boxShadow: 'none',
    '&:hover': {
      backgroundColor: Purple[500],
    },
  }, buttonBook: {
    color: '#ffff',
    backgroundColor: Orange[300],
    boxShadow: 'none',
    '&:hover': {
      backgroundColor: Orange[500],
    },
  },
  root: {
    overflowY: 'auto'
  }
})

class ModalBook extends Component {

  state = {
    country: 0,
    openCountry: false,
  }

  handleCloseCountry = country => {
    this.setState({ country, openCountry: false })
  }
  handleOpenCountry = () => {
    this.setState({ openCountry: true })
  }

  render () {
    let { classes, open, other, handleClose, count, price, handleChange, phone, mail, name, handleSubmit, isError } = this.props
    let { openCountry, country } = this.state
    return (<Modal
      aria-labelledby="simple-modal-title"
      aria-describedby="simple-modal-description"
      open={open}
      onClose={handleClose}
      classes={{ root: classes.root }}
    >
      <div className={classes.paper}>
        <form onSubmit={handleSubmit}>
          <Typography variant="h6" className={classes.title} id="modal-title">
            Стать участником
          </Typography>

          <Typography variant="subtitle2" id="simple-modal-description">
            Психиатрическая №16
          </Typography>

          <Typography variant="body2" className={classes.text}>
            Харьков. Сумская 13
          </Typography>

          <Typography variant="body2" className={classes.text}>
            Пятница. <strong style={{ fontWeight: 500 }}>10 января, 16:40 - 17:40</strong>
          </Typography>

          <Field label={'Имя'} handleChange={handleChange('name')} value={name} error={isError && !name}/>
          <Grid container spacing={24}>
            <Grid item xs={3}>
              <SelectCountries handleOpen={this.handleOpenCountry}
                               handleClose={this.handleCloseCountry}
                               open={openCountry} index={country}/>
            </Grid>
            <Grid item xs={9}>
              <Field label={'Телефон'} type={'tel'} handleChange={handleChange('phone', country)}
                     value={'+' + Countries[country].phoneCode + phone} error={isError && !phone}/>
            </Grid>
          </Grid>
          <Field label={'Email'} type={'email'} handleChange={handleChange('mail')} value={mail}
                 error={isError && !mail}/>
          <Field label={'Кол-во игроков'} handleChange={handleChange('count')} type={'number'}
                 value={count.toString()}/>

          {other.map(o => <FormControlLabel key={o.name} className={classes.checkBox} control={
            <Checkbox checked={o.isActive} className={classes.checked} value={o.price.toString()}/>}
                                            label={o.name}
          />)}

          <Typography variant="h6" style={{ marginBottom: 8 }}>
            Цена игры: {price}грн
          </Typography>

          <Typography variant="body2" className={classes.text}>
            Нажимая на кнопку "Забронировать"я принимаю условия <a className={classes.link} href={'/123'}>"Политики
            конфиденциальности"</a> и даю
            согласие на
            обработку
            персональных данных
          </Typography>
          <Grid container spacing={24} style={{ marginTop: 8 }}>
            <Grid item xs={5}>
              <Button variant="contained" fullWidth={true} className={classes.buttonBuy}>
                Оплатить
              </Button>
            </Grid>
            <Grid item xs={7}>
              <Button variant="contained" type={'submit'} fullWidth={true} className={classes.buttonBook}>
                Забронировать
              </Button>
            </Grid>
          </Grid>
        </form>
      </div>

    </Modal>)
  }
}

ModalBook.propTypes = {
  open: PropTypes.bool,
  handleClose: PropTypes.func,
  other: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string,
    price: PropTypes.number,
    isActive: PropTypes.bool
  })),
  name: PropTypes.string,
  phone: PropTypes.string,
  mail: PropTypes.string,
  count: PropTypes.number,
  price: PropTypes.number,
  isError: PropTypes.bool,
  classes: PropTypes.object.isRequired,
  handleChange: PropTypes.func,
  handleSubmit: PropTypes.func
}

ModalBook.defaultProps = {
  other: []
}

export default withStyles(styles)(ModalBook)