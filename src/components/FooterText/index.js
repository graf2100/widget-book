import React      from 'react'
import Grid       from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'

export default function () {
  return (<Grid item xs={8}>
    <Typography component="p"><span style={{color: '#ef5350', marginLeft: '2.5rem'}}>*</span> Цена игры указана за
      одного человека.
      Обезательно необходимо
      выбрать количество игроков при заполнении заявки</Typography>
  </Grid>)
}