import React     from 'react'
import PropTypes from 'prop-types'

import { withStyles } from '@material-ui/core/styles'
import TextField      from '@material-ui/core/TextField'
import MenuItem       from '@material-ui/core/MenuItem'
import Purple         from '@material-ui/core/colors/purple'

const styles = theme => ({
  formControl: {
    marginTop: 16,
    marginBottom: 8,
    minWidth: 120,
    width: '100%'
  },
  checked: {
    color: Purple[600],
    '&$checked': {
      color: Purple[500],
    },
  },
  textField: {
    width: '100%',
    '&$cssFocused': {
      borderColor: Purple[500],
    },
  },

})

function ISelect (props) {
  let {label, classes} = props
  return (
    <TextField
      select
      label={label}
      className={classes.textField}
      value={''}
      InputLabelProps={{
        classes: {
          root: classes.cssLabel,
          focused: classes.cssFocused,
        },
      }}
      InputProps={{
        classes: {
          root: classes.cssOutlinedInput,
          focused: classes.cssFocused,
          notchedOutline: classes.notchedOutline,
        },
      }}
      SelectProps={{
        MenuProps: {
          root: classes.cssOutlinedInput,
          className: classes.menu,
          focused: classes.cssFocused,
        },
      }}
      margin="normal"
      variant="outlined"
    >
      <MenuItem key={2} value={2}>
        2
      </MenuItem>
      <MenuItem key={3} value={3}>
        3
      </MenuItem>
    </TextField>
  )
}

ISelect.propTypes = {
  label: PropTypes.string,
  classes: PropTypes.object.isRequired,
}

ISelect.defaultProps = {}

export default withStyles(styles)(ISelect)
