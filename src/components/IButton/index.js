import React          from 'react'
import PropTypes      from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import Button         from '@material-ui/core/Button'
import Purple         from '@material-ui/core/colors/purple'

const styles = theme => ({
  button: {
    color: '#ffff',
    backgroundColor: Purple[300],
    boxShadow: 'none',
    '&:hover': {
      backgroundColor: Purple[500],
    },
  },
})

function IButton (props) {
  const {classes, value, isActive, onOpen} = props
  return (<Button variant="contained" onClick={onOpen} fullWidth={true} disabled={!isActive} className={classes.button}>
    {value}
  </Button>)
}

IButton.propTypes = {
  classes: PropTypes.object.isRequired,
  value: PropTypes.number,
  color: PropTypes.string,
  isActive: PropTypes.bool,
  onOpen: PropTypes.func
}

IButton.defaultProps = {
  value: 1000,
  color: Purple[300],
  isActive: true,
}

export default withStyles(styles)(IButton)