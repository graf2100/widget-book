import moment from 'moment'

const getTimeFromMins = m => moment.utc().hours(m / 60 | 0).minutes(m % 60 | 0).format('HH:mm')

const unique = arr => {
  let obj = {}

  for (let i = 0; i < arr.length; i++) {
    let str = arr[i]
    obj[str] = true
  }

  return Object.keys(obj)
}
const createItem = (day, time, isActive, price) => ({ day, time, isActive, price })
const createLine = (min, arr) => {
  let line = []
  let scheme = []
  arr.forEach(time => {
    for (let i = 0; i < time.count_game; i++) {
      if (i === 0) {
        line.push(time.time_open)
        time.days.split(',').map(day => parseInt(day)).forEach(day => {
          scheme.push(createItem(day, getTimeFromMins(time.time_open), true, (time.player_price * min) + time.price))
        })
      } else {
        line.push(time.time_open + ((time.time_play + time.time_break) * (i)))
        time.days.split(',').map(day => parseInt(day)).forEach(day => {
          scheme.push(createItem(day, getTimeFromMins(time.time_open + ((time.time_play + time.time_break) * (i))), true, (time.player_price * min) + time.price))
        })
      }
    }
  })
  line = line.sort((a, b) => a - b).map(i => getTimeFromMins(i))
  return { periods: unique(line), scheme }
}

export { createLine }