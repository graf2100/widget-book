import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'

import * as moment from 'moment'
import io from 'socket.io-client'

import { withStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import CircularProgress from '@material-ui/core/CircularProgress'

import { Border, DateBar, FooterText, Modal, TimeLine } from './components'
import Countries from './utilities/countries'
import { createLine } from './utilities/timeline'
import Purple from '@material-ui/core/colors/purple'

const styles = theme => ({
  root: {
    marginTop: theme.spacing.unit * 10,
  },
  progress: {
    marginTop: '25ch',
    marginLeft: 'calc(50% - 37.5px)',
    color: Purple[300],
  },
})

const formatMoment = 'DD.MM.YYYY'
const createDays = () => ['', '', '', '', '', '', ''].map((d, index) => parseInt(moment().add(index, 'd').format('e')) + 1)

class App extends Component {
  state = {
    room: 2,
    open: false,
    name: '',
    phone: '',
    mail: '',
    count: 2,
    price: 0,
    addNumber: '+380',
    dateNow: moment().format('L'),
    dateFinal: moment().add(2, 'M').format('L'),
    dateActive: moment().format('L'),
    isError: false,
    isLoad: true,
    periods: [],
    scheme: [],
    days: createDays()
  }

  nextWeek = () => {
    let { dateActive, dateFinal } = this.state

    dateActive = moment(dateActive, formatMoment).add(1, 'w').format('L')

    if (moment(dateActive, formatMoment).isSameOrBefore(moment(dateFinal, formatMoment))) {
      this.setState({ dateActive })
    }
  }

  prevWeek = () => {
    let { dateActive, dateNow, } = this.state

    dateActive = moment(dateActive, formatMoment).subtract(1, 'w')

    if (moment(dateActive, formatMoment).isSameOrAfter(moment(dateNow, formatMoment))) {
      this.setState({ dateActive })
    }
  }

  handleChange = (name, country) => event => {
    if (name === 'phone') {
      let p = event.target.value.replace('+' + Countries[country].phoneCode, '')
      p = p.replace(/[^-0-9]/gim, '')
      this.setState({
        phone: p,
        addNumber: '+' + Countries[country].phoneCode
      })
    } else {
      this.setState({
        [name]: event.target.value,
      })
    }
  }

  handleOpen = () => {
    this.setState({ open: true })
  }

  handleClose = () => {
    this.setState({
      open: false,
      name: '',
      phone: '',
      mail: '',
      count: 3,
      price: 200,
    })
  }
  handleSubmit = e => {
    e.preventDefault()

    let { name, mail, phone } = this.state

    if (!name || !mail || !phone) {
      this.setState({ isError: true })
    } else {
      this.setState({
        name: '',
        phone: '',
        mail: '',
        count: 2,
        price: 0,
        isError: false
      })
      this.handleClose()
    }
  }

  componentDidMount () {
    const socket = io('ws://localhost:4000', { transports: ['websocket'] })

    socket.on('connect', () => {
      this.setState({ isLoad: false })
      socket.emit('room', this.state.room)
    })

    socket.on('room', data => {
      let {
        timelines, players_min
      } = data

      let { periods, scheme } = createLine(players_min, timelines)

      this.setState({ periods, scheme, isLoad: !periods })
    })

    socket.on('disconnect', () => {
      this.setState({ isLoad: true })
    })
  }

  render () {
    const { classes } = this.props
    const { open, name, phone, mail, count, price, dateNow, dateActive, isError, isLoad, periods, scheme, days } = this.state
    return isLoad ? (<CircularProgress className={classes.progress} size={75}/>) : (
      <Fragment>
        <Grid container direction="row" justify="center" alignItems="center" spacing={16}
              className={classes.root}>
          <DateBar now={moment(dateNow, formatMoment)} active={moment(dateActive, formatMoment)} next={this.nextWeek}
                   prev={this.prevWeek}/>
          <TimeLine periods={periods}/>
          <Border handleOpen={this.handleOpen} periods={periods} scheme={scheme} days={days}/>
          <FooterText/>
        </Grid>
        <Modal open={open} name={name} phone={phone} mail={mail} other={[{ name: 'aaa', price: 100, isActive: true }]}
               count={+count} price={+price} isError={isError}
               handleClose={this.handleClose} handleChange={this.handleChange}
               handleSubmit={this.handleSubmit}/>
      </Fragment>
    )
  }
}

App.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(App)